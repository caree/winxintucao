

var _                  = require('underscore');
var timeFormater 	   = require('./timeFormat').getCurrentTime;


exports.classManageIndex = function(req, res){
	res.render('classManage');
}
exports.classManageLeftIndex = function(req, res){
	res.render('classManageLeft');
}
exports.classManageTopIndex = function(req, res){
	res.render('classManageTopIndex');
}
exports.classManageClassIndex = function(req, res){
	res.render('classManageClassIndex');
}
exports.classManageChangepwdIndex = function(req, res){
	res.render('classManageChangepwdIndex');
}
exports.classApplyIndex = function(req, res){
	res.render('classApplyIndex');
}
exports.classApply = function(req, res){
	if(checkAuthenrity(req)){
		var classID = req.body.classID;
		var ghRegName = req.body.ghRegName;
		var ghName = req.body.ghName;
		var tip = req.body.tip;
		var creator = req.body.creator;
		return classDBFindOne({classID: classID}).then(function(_class){
			if(_class != null) throw new Error('duplicated');
			var newClassApplied = {classID: classID, secret: '111', ghRegName: ghRegName, ghName: ghName, tip: tip, status: 'false', creator: creator};
			return classDBInsert(newClassApplied)
			.then(function(_newClass){
				console.log('add class ok'.info);
				classMemory.push(newClassApplied);
				res.send('ok');
			})
		}).catch(function(err){
			console.log('error => classApply ' + err.message);
			res.send('error');
		})		
	}else{
		res.send('error');
	}
}
exports.classList = function(req,res){
	if(checkAuthenrity(req)){
		return classDBFind({}).then(function(_classes){
			var classes = _.chain(_classes)
						.filter(function(_class){
							return _class.classID != 'admin';})
						.map(function(_class){
							return _.omit(_class, 'secret');})
						.value();
			// var classes = _
			res.send(JSON.stringify(classes));
		})		
	}else{
		res.send('');
	}
}
exports.classNameList = function(req, res){
	return classDBFind({}).then(function(_classes){
			var classes = _.chain(_classes)
						.map(function(_class){
							return _.pick(_class, 'ghRegName');})
						.value();
			classes.push({ghRegName: 'admin'});
			res.send(JSON.stringify(classes));
		}).catch(function(err){
			console.log(('error => classNameList ' + error.message).error);
			res.send('error');
		})
}
function checkAuthenrity(req){
	if(req.session.classID == 'admin'){
		return true;
	}else{
		console.log('not logined!!!'.error);
		return false;
	} 
}
exports.setClassStatusOk = function(req, res){
	if(checkAuthenrity(req)){
		var classID = req.body.classID;
		var status  = req.body.status;
		classDBFindOne({classID: classID}).then(function(_class){
			if(_class == null) throw new Error('noClass');
			return classDBUpdate({classID: classID}, {$set: {status: status}})
			.then(function(_num){
				if(_num <= 0) throw new Error('error');

				var classSet = _.findWhere(classMemory, {classID: classID})
				if(classSet != null){
					classSet.status = status;
				}else throw new Error('exceptionInUpdateMemoClass');
				res.send('ok');
			})
		}).catch(function(err){
			console.log(('error => setClassStatusOk ' + err.message).error);
			res.send('error');
		})	
	}else{
		res.send('error');
	}
}
exports.deleteClass = function(req,res){
	if(checkAuthenrity(req)){
		var classID = req.body.classID;
		return classDBRemove({classID: classID}).then(function(_num){
			if(_num > 0){
				res.send('ok');
			}else throw new Error('error');
		}).catch(function(error){
			console.log(('error => deleteClass ' + error.message).error);
			res.send('error');
		})	
	}else{
		res.send('error');
	}
}
exports.postNewPassword = function(req, res){
	var ghID = req.session.classID;
	var currentPassword = req.body.currentPassword;
	var newpassword = req.body.newpassword;

	return classDBFindOne({classID: ghID}).then(function(_class){
		if(_class == null) throw new Error('noClass');
		if(_class.secret == currentPassword){
			// _classes.secret = newpassword;
			return classDBUpdate({classID: ghID}, {$set: {secret: newpassword}});
		}else{
			throw new Error('password error');
		}
	}).catch(function(err){
		console.log(('error => postNewPassword ' + err.message).error);
		res.send('error');
	})
}

// exports.checkLogin = function(req, res){
// 	var regName = req.body.username;
// 	var pwd  = req.body.password;
// 	return classDBFindOne({ghRegName: regName, secret: pwd}).then(function(_class){
// 		if(_class == null) throw new Error('noClass');
// 		if(req.session.ghRegName == regName){
// 			req.session.views ++;
// 		}else{
// 			req.session.ghRegName = regName;
// 			req.session.views = 1;		
// 		}		
// 		req.session.classID = _class.classID;
// 		res.send('ok');
// 	}).catch(function(err){
// 		console.log(('error => checkLogin ' + err.message).error);
// 		res.send('error');
// 	})
// };





