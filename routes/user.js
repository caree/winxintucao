

var _                  = require('underscore');
var timeFormater 	   = require('./timeFormat').getCurrentTime;


exports.userManageIndex = function(req, res){
	res.render('userManage');
}
exports.userManageLeftIndex = function(req, res){
	res.render('userManageLeft');
}
exports.userManageTopIndex = function(req, res){
	res.render('userManageTopIndex');
}
exports.userManageUserIndex = function(req, res){
	res.render('userManageUserIndex');
}
exports.userManageChangepwdIndex = function(req, res){
	res.render('userManageChangepwdIndex');
}
exports.setSpeakerIndex = function(req, res){
	res.render('setSpeakerIndex');
}
exports.setUserStateIndex = function(req, res){
	res.render('setUserStateIndex');
}

exports.postNewPassword = function(req, res){
	var ghID = req.session.classID;
	var currentPassword = req.body.currentPassword;
	var newpassword = req.body.newpassword;

	return classDBFindOne({classID: ghID}).then(function(_class){
		if(_class == null) throw new Error('noClass');
		if(_class.secret == currentPassword){
			// _classes.secret = newpassword;
			return classDBUpdate({classID: ghID}, {$set: {secret: newpassword}})
					.then(function(_num){
						if(_num > 0){
							res.send('ok');
						}else throw new Error('password error');
					});
		}else{
			throw new Error('password error');
		}
	}).catch(function(err){
		console.log(('error => postNewPassword ' + err.message).error);
		res.send('error');
	})
}

exports.blockSpeak = function(req ,res){
	var userName = req.body.userName;
	var blockState = req.body.blockState;
	var ghID = req.session.classID;

	return setBlockState(ghID, userName, blockState).then(function(_msg){
		res.send('ok');
	}).catch(function(err){
		console.log(('error => blockSpeak ' + err.message).error);
		res.send('error');
	})
	// return classDBFindOne({classID: ghID}).then(function(_class){
	// 	if(_class == null) throw new Error('noClass');
	// 	return userDBFindOne({userName: userName}).then(function(_user){
	// 		if(_user == null) throw new Error('noUser');
	// 		return userDBUpdate({userName: userName}, {$set: {blocked: blockState}})
	// 		.then(function(){
	// 			res.send('ok');
	// 		});
	// 	})
	// }).catch(function(err){
	// 	console.log(('error => blockSpeak ' + err.message).error);
	// 	res.send('error');
	// })
}
exports.setBlockState = setBlockState;
function setBlockState(_ghID, _userName, _blockState){
	return classDBFindOne({classID: _ghID}).then(function(_class){
		if(_class == null) throw new Error('noClass');
		return userDBFindOne({userName: _userName}).then(function(_user){
			if(_user == null) throw new Error('noUser');
			if(_blockState == null){
				_blockState = (_user.blocked == 'true') ? 'false':'true';
			}
			return userDBUpdate({userName: _userName}, {$set: {blocked: _blockState}})
			.then(function(){
				return 'ok';
			});
		})
	});

}
exports.userList4BlockState = function(req, res){
	var ghID = req.session.classID;
	return userDBFind({classID: ghID}).then(function(_users){
		var users = _.map(_users, function(_user){
			return {userName: _user.userName, state: _user.blocked == 'true'? '已禁言':'正常'};
		});		
		console.dir(users);
		res.send(JSON.stringify(users));
	}).catch(function(err){
		console.log(('error => userList4BlockState ' + err.message).error);
		res.send(JSON.stringify([]));
	})
}

exports.setSpeaker = function(req ,res){
	var speakerName = req.body.userName;
	var speakerState = req.body.speakerState;
	var ghID = req.session.classID;
	return classDBFindOne({classID: ghID}).then(function(_class){
		if(_class == null) throw new Error('noClass');
		return userDBFindOne({userName: speakerName})
		.then(function(_user){
			if(_user == null) throw new Error('noUser');
			return userDBUpdate({userName: speakerName}, {$set: {isSpoker: speakerState}})
			.then(function(){
				res.send('ok');
			})
		})
	}).catch(function(err){
		console.log(('error => setSpeaker ' + err.message).error);
		res.send('error');
	})	
}
exports.userList4Speaker = function(req, res){
	var ghID = req.session.classID;
	return userDBFind({classID: ghID}).then(function(_users){
		var users = _.map(_users, function(_user){
			return {userName: _user.userName, state: _user.isSpoker == 'true'? '发言人':'非发言人'};
		});
		console.dir(users);
		res.send(JSON.stringify(users));		
	}).catch(function(err){
		console.log(('error => userList4Speaker ' + err.message).error);
		res.send(JSON.stringify([]));
	})
}

// exports.userList4Login = function(req, res){
// 	res.send(JSON.stringify(_.chain(ghIDList).map(function(_ghIDInfo){
// 		return {ghRegName: _ghIDInfo.ghRegName, creator: _ghIDInfo.creator};
// 	}).value()));
// }
exports.checkLogin = function(req, res){
	var regName = req.body.username;
	var pwd  = req.body.password;
	console.log('name: ' + regName + ' pwd: ' + pwd);
	if(regName == 'admin'){
		return classDBFindOne({classID: 'admin'}).then(function(_class){
			if(_class == null || _class.secret != pwd){
				res.send('error');
				return;
			}

			req.session.classID = 'admin';
			res.send('ok');
		}).catch(function(err){
			console.log(('error => checkLogin ' + err.message).error);
			res.send('error');
		})
	}else{
		return classDBFindOne({ghRegName: regName, secret: pwd}).then(function(_class){
			if(_class == null) throw new Error('noClass');
			if(req.session.ghRegName == regName){
				req.session.views ++;
			}else{
				req.session.ghRegName = regName;
				req.session.views = 1;		
			}		
			req.session.classID = _class.classID;
			res.send('ok');
		}).catch(function(err){
			console.log(('error => checkLogin ' + err.message).error);
			res.send('error');
		})		
	}

};





