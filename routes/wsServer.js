
var http = require('http');
var WebSocketServer = require('ws').Server;
var _ = require("underscore");
var querystring = require('querystring');
var url = require('url');

var server;
var wss;
var clients = [];

ep.tail('broadcastInfo', broadcastInfo);

function broadcastInfo(data){
	console.log('broadcastInfo =>');
	_.each(clients, function(_client){
        if(_client.classID == data.classID){
			var str = JSON.stringify([data.tucao]);
			console.log('=> client: '+_client.classID + ' data: '+ str);
	        _client.ws.send(str);
        }
		// _client.send(JSON.stringify([data.tucao]));
	});	
}
exports.startWebSocketServer = function(app){

	server = http.createServer(app);
	wss = new WebSocketServer({server : server});
	console.log('wss ok =>'.data);
	wss.on('connection', function(ws){
	    console.log('new websocket connected'.info);
	    //console.log(ws.upgradeReq.url);// /tucao/u1
	    var obj = url.parse(ws.upgradeReq.url, true);
	    if(obj != null && obj.query.hasOwnProperty('user') && obj.query.user != null){
		    console.log('add user: ' + obj.query.user);
			clients.push({ws: ws, classID: obj.query.user, url: ws.upgradeReq.url});
	    }else{
	    	console.log('error => startWebSocketServer: the user is not defined!'.error)
	    	return;
	    }
		
		// clients.push(ws);
		
	    // 连接成功后将当前所有存在的标签都发送给客户端
		ws.on('open', function(){
		    console.log('ws open'.info);
		    // var str = JSON.stringify(tucaoMessageList);
		    // console.log(str);
		    // ws.send(str);
		  });
		
		ws.on('message', function(msg){
		    console.log('message => '+ msg);
		    // var objMsg  = JSON.parse(msg);

		  });
		
		ws.on('close', function(){
			console.log('ws close =>'.warn);
		    clients = _.filter(clients, function(_client){
		    	// return _client.ws != ws;
				return _client.url != ws.upgradeReq.url;
		    });
		    // var index = clients.indexOf(ws);
		    // if(index >= 0){
			    // clients.splice(index, 1);
			    // console.log('close =>'.warn);
		    // }
		  });
		  
		});	
	
	return server;
}




