

var _                  = require('underscore');
var Q                  = require('q');
var xml2js             = require('xml2js');
var weixin = require('./weixinInterface');
var xml = require('xml');
var Message = require('./Message');
var NewsMessage = require('./NewsMessage');
var checkMsgList = require('./importMsg').checkMsgList;
var timeFormater 	   = require('./timeFormat').getCurrentTime;
var userModule = require('./user');

var parser = new xml2js.Parser();
parseXml = Q.nbind(parser.parseString, parser);

var regSetName = /###[\w\W]+###/;//如果内容类似于 ###姓名###，就认为是注册用户名，将注册的名字与id绑定存储到本地
var regGetClassTip = /[@]{3,50}/;// @@@
var regSysTip  = /[\?？]{3,50}/;// ??? 中英文符合
var regSetClassTip = /[\:：]{3,3}[\w\W]+/;// :::
var regBlockSpeaking = /[!！]{3}[\w\W]+/;//如果内容类似于此，将对该用户禁言或者解除禁言
var commanTypeFilter = [
	{regex: regSetName, type: 'setName'},
	{regex: regGetClassTip, type: 'getClassTip'},
	{regex: regSetClassTip, type: 'setClassTip'},
	{regex: regSysTip,  type: 'sysTip'},
	{regex: regBlockSpeaking, type: 'blockSpeaking'}
];

initialDB();

exports.test = function(req, res){
	console.log('test =>');
	res.send('ok');
}
exports.tucaoIndex = function(req, res){
	res.render('index');
}
exports.tucaoAjax = function(req, res){
	res.render('indexajax');
}

exports.getTucao = function(req, res){
	var timeStamp = req.body.timeStamp;
	var list = _.chain(tucaoMessageList).filter(function(_tucao){
		return _tucao.timeStamp > timeStamp;
	}).value();
	console.log('later than ' + timeStamp);
	console.dir(list);
	res.send(JSON.stringify(list));
}
exports.loginIndex = function(req, res){
	res.render('login');
}

exports.weinCheck = function(req, res){
	var signature = req.query.signature;
	var timestamp = req.query.timestamp;
	var nonce = req.query.nonce;
	var echostr = req.query.echostr;
	if(weixin.isLegel(signature, timestamp, nonce, token) == true){
		res.send(echostr);
	}else{
		res.send('');
	}	
}
exports.index = function(req, res){

}
exports.receiveMsg = function(req, res){
	console.log('receiveMsg =>'.info);
	console.log(req.rawBody);
	parseComingInMessage(req.rawBody)
	.then(function(_receivedMsg){
		var classInfo = _.findWhere(classMemory, {classID: _receivedMsg.ToUserName});
		if(classInfo == null) throw new Error('noClass');
		if(classInfo.status == 'false') throw new Error('classBlocked');
		return dispatchCommand(_receivedMsg, req, res);
	}).catch(function(error){
		console.log(error.message.error);
		res.send('');
	})
	return;
}
function dispatchCommand(_receivedMsg, req, res){
	var message = new Message(_receivedMsg.FromUserName, _receivedMsg.ToUserName, _receivedMsg.CreateTime, _receivedMsg.MsgType);
	switch(getCommandType(_receivedMsg.Content)){
		case 'setName':
			// 通知设置结果
			return stroreUserInfo(_receivedMsg.Content, _receivedMsg.FromUserName, _receivedMsg.ToUserName)
			.then(function(_result){
				message.Content = _result;
				res.send(buildXml(message.getPrepareXmlBuilding()));
			}).catch(function(error){
				console.log(('error => setName ' + error.message).error);
				message.Content = error.message;
				res.send(buildXml(message.getPrepareXmlBuilding()));
			});
		break;
		case 'getClassTip':
			return classDBFindOne({classID: _receivedMsg.ToUserName}).then(function(_class){
				if(_class == null) throw new Error('noClass');
				message.Content = _class.tip;
				res.send(buildXml(message.getPrepareXmlBuilding()));
			}).catch(function(err){
				console.log(('error => getClassTip ' + err.message).error);
				message.Content = '';
				res.send(buildXml(message.getPrepareXmlBuilding()));
			});
		break;
		case 'setClassTip':
			return classDBFindOne({classID: _receivedMsg.ToUserName}).then(function(_class){
				if(_class == null) throw new Error('noClass');
			}).then(function(){
				return userDBFindOne({userID: _receivedMsg.FromUserName, isSpoker: 'true'})
				.then(function(_user){
					if(_user == null){
						console.log((_receivedMsg.FromUserName + ' is not speaker').warn);
						res.send('');
						return;
					}
					// var index1 = _receivedMsg.Content.indexOf(':::');
					var tip = _receivedMsg.Content.substr(3);
					return classDBUpdate({classID: _receivedMsg.ToUserName}, {$set: {tip: tip}})
					.then(function(_num){
						message.Content = '设置成功';
						res.send(buildXml(message.getPrepareXmlBuilding()));
					})					
				})
			}).catch(function(err){
				console.log(('error => setClassTip ' + err.message).error);
				res.send('');
			})
		break;
		case 'sysTip':
			message.Content = helpInfo;
			res.send(buildXml(message.getPrepareXmlBuilding()));
		break;
		case 'blockSpeaking':
			var userName = _receivedMsg.Content.substr(3);
			return (isSpoker(_receivedMsg.ToUserName, _receivedMsg.FromUserName)).then(function(_isSpoker){
				if(_isSpoker == true){
					return userModule.setBlockState(_receivedMsg.ToUserName, userName, null).then(function(){
						message.Content = '操作成功';
						res.send(buildXml(message.getPrepareXmlBuilding()));
					}).catch(function(err){
						console.log(('error => blockSpeaking ' + err.message).error);
						message.Content = '操作失败';
						res.send(buildXml(message.getPrepareXmlBuilding()));
					})				
				}else{
					message.Content = '没有权限';
					res.send(buildXml(message.getPrepareXmlBuilding()));
				}					
			})
		break;
		case 'tucao':
			// 查看是否被禁言
			return classDBFindOne({classID: _receivedMsg.ToUserName}).then(function(_class){
				if(_class == null) throw new Error('noClass');
			}).then(function(){
				return userDBFindOne({userID: _receivedMsg.FromUserName, blocked: 'false'})
				.then(function(_user){
					if(_user == null){
						console.log('no user or user is blocked'.info);
						res.send('');
					}else{
						var tucaoContent = _receivedMsg.Content;
						var timeStamp = timeFormater();
						var newTucao = {FromUserName: _receivedMsg.FromUserName, classID: _receivedMsg.ToUserName, Content: tucaoContent, timeStamp: timeStamp};
						newTucao.name = _user.userName;
						console.log((_user.userName + ' of class '+ newTucao.classID +' says:'+ tucaoContent).data);
						ep.emit('broadcastInfo', {tucao: newTucao, classID: _receivedMsg.ToUserName});
						res.send('');				
					}
				})
			}).catch(function(error){
				console.log(error.message.error);
				res.send('');
			})
		break;
	}
}
//必须设置了名称才能加入班级
function stroreUserInfo(_rawInfo, _userID, _classID){
	// var classInfo = _.findWhere(ghIDList, {ghID: _classID});
	return classDBFindOne({classID: _classID}).then(function(_classInfo){
		if(_classInfo == null){
			throw new Error('你好像走错了地方！');
			return;
		}
		var index1 = _rawInfo.indexOf('###');
		var index2 = _rawInfo.lastIndexOf('###');
		var name = _rawInfo.substr(index1 + 3, index2 - index1 - 3);
		if(name.length <= 0 || name.length > 16) throw new Error('您设置的名字不符合规范！');
		console.log('name:' + name + ' classID:' + _classID);
		return userDBFindOne({userName: name, classID: _classID}).then(function(_user){
			if(_user != null) throw new Error('已经有人使用这个名字了');
		}).then(function(){
			// return userDBRemove({userID: _userID});
			return userDBFindOne({userID: _userID});
		}).then(function(_user){
			if(_user == null){
				return userDBInsert({userID: _userID, userName: name, classID: _classID, isSpoker: 'false', blocked:'false'})
				.then(function(_user){
					console.log(('id ' + _user.userID + ' 与姓名 ' + _user.userName + ' 绑定成功').info);
					return '设置姓名成功， 欢迎您加入' + _classInfo.ghName;
				});
			}else{
				return userDBUpdate({userID: _userID}, {$set:{userName: name}})
				.then(function(_num){
					if(_num > 0){
						console.log(('id ' + _userID + ' 与姓名 ' + name + ' 绑定成功').info);
						return '设置姓名成功， 欢迎您加入' + _classInfo.ghName;
					}else{
						throw new Error('发生异常: userDBUpdate userName')
					}
				});
			}
		})
	})
}
function isSpoker(_classID, _speakerID){
	return userDBFindOne({classID: _classID, userID: _speakerID, isSpoker: 'true'}).then(function(_user){
		if(_user == null) return false;
		return true;
	})
}
function getCommandType(_content){
	console.log('getCommandType => ' + _content);
	var commandTypeInfo = _.chain(commanTypeFilter).find(function(_typeInfo){
		return _typeInfo.regex.test(_content);
	}).value();
	
	if(commandTypeInfo != null){
		console.dir(commandTypeInfo);
		return commandTypeInfo.type;
	}else return 'tucao';
}
function initialDB(){
	classDBFind({}).then(function(_classes){
		if(_.size(_classes) <= 0){
			_.each(classes, function(_class){
				classDBInsert(_class);
			})
		}else{
			console.log('db has classes =>'.info);
			console.dir(_classes);
			_.each(_classes, showUsers);
		}
	}).then(function(){
		classDBFind({}).then(function(_classes){
			classMemory = _classes;
		})
	})
	
}
function showUsers(_classInfo){
	console.log((_classInfo.ghRegName + ' =>').info);
	// userDBFind({}).then(function(_users){
		// console.dir(_users);
	// })
	userDBFind({classID: _classInfo.classID}).then(function(_users){
		console.dir(_users);
	})
}


function checkMsgState(_content, _stateList){
	var state = _.chain(_stateList).filter(function(_state){
		return _state.separator != null;
	}).find(function(_state){
		return _content.indexOf(_state.separator) > 0;
	}).value();
	if(state != null) return state 
	else return _stateList[0];
}

exports.parseComingInMessage = parseComingInMessage;
function parseComingInMessage(_msg){
	return parseXml(_msg).then(function(_result){
		var f = function(_obj){
			return function(_property){
				return _.has(_obj, _property)
			};
		};
		if(f(_result)('xml')){
			var resultxmlFunc = f(_result.xml);
			if(resultxmlFunc('ToUserName') && resultxmlFunc('FromUserName') && resultxmlFunc('CreateTime')
				&& resultxmlFunc('MsgType') && resultxmlFunc('MsgId') && resultxmlFunc('Content')){
				var resultxml = _result.xml;
				return new Message(resultxml.ToUserName[0], resultxml.FromUserName[0], resultxml.CreateTime[0], resultxml.MsgType[0], resultxml.Content[0], resultxml.MsgId[0]);
			}else{
				throw new Error('propertyError');
			} 
		}else{
			throw new Error('DataErrorXml');
		}
	})
}
exports.buildXml = buildXml;
function buildXml(_obj){
	return xml(_obj);
}

function getAccessID(){
	var url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=ssor@qq.com&secret=0785150790";
	httpRequestGet(url).then(function(_response){
		// console.dir(_response);
		// console.log('*****************************')
		// console.dir(_response[0]);
		// console.log('*****************************')
		var data = _response[0].body;
		console.log(data);
		try{
			// {"access_token":"ACCESS_TOKEN","expires_in":7200}
			var list = JSON.parse(data);
			access_token = list.access_token;

		}catch(e){
			console.log(data.error);
		}
	}).catch(function(error){
		console.log('error <= getAccessID '.error);
	});
}
//*********************************************************
exports.overview = function(req, res){
	res.render('overview');
};
exports.indexAPI = function(req, res){
	res.render('indexAPI', {title:'API Test'});
}
