
/**
 * Module dependencies.
 */
helpInfo            = 
    '1.发送 ??? 查看系统使用说明;\n'+
    '2.发送 ###+姓名+### 加入本班级同时设置自己发言时的名字;\n'+
    '3.直接发送内容吐槽;\n'+
    '4.发送 @@@ 查看课程信息;\n'+
    '5.管理员发送 :::+内容, 设置课程信息;\n'+
    '6.发送 !!!+姓名 对其禁言或者解除禁言;';


token               = 'nodewebgis';
access_token        = null;
tucaoMessageList = [];
ep = null;
zhangquanzhi110ID = 'gh_f84a5b14484d';
webgisID = 'gh_1d62fe1627d4';

// ghIDList = [
//   {
//     ghID: zhangquanzhi110ID,
//     secret: '111',
//     ghRegName: '张全志的课堂',//微信平台注册名称
//     ghName: '物流信息课',
//     tip : '',
//     creator: '张全志',
//     userList: [
//       {userName: 'user1', isSpoker: 'false', blocked: 'false'},
//       {userName: 'user2', isSpoker: 'false', blocked: 'false'},
//       {userName: 'user3', isSpoker: 'true', blocked: 'false'}
//     ]},

//   {
//     ghID: webgisID,
//     secret: '111',
//     ghRegName: '运输监控平台',//微信平台注册名称
//     ghName: '管理信息系统课',
//     tip : '',
//     creator: '张全志2',
//     userList: [
//       {userName: 'user1', isSpoker: 'false', blocked:'false'},
//       {userName: 'user2', isSpoker: 'false', blocked:'false'},
//       {userName: 'user3', isSpoker: 'true', blocked:'false'}
//     ]}
// ];
classes = [
  {
    classID: webgisID,
    secret: '111',
    ghRegName: '运输监控平台',//微信平台注册名称
    ghName: '管理信息系统课',
    tip : '',
    status: 'false',//状态审查
    creator: '张全志'},
  {
    classID: zhangquanzhi110ID,
    secret: '111',
    ghRegName: '张全志的课堂',//微信平台注册名称
    ghName: '物流信息课',
    status: 'false',
    tip : '',
    creator: '张全志'},
  {
      classID: 'admin',
      secret: '111'}];

var colors          = require('colors');
colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});
classMemory = [];//将班级设置暂存在缓存中
var EventProxy = require('eventproxy');
ep = new EventProxy();

var Q               = require('q');

var Datastore          = require('nedb');

// {userID: '', userName: 'user1', isSpoker: 'false', blocked:'false', classID: webgisID},
var userDB  = new Datastore({ filename: 'user.db', autoload: true });
userDBFind = Q.nbind(userDB.find, userDB);
userDBFindOne = Q.nbind(userDB.findOne, userDB);
userDBRemove = Q.nbind(userDB.remove, userDB);
userDBInsert = Q.nbind(userDB.insert, userDB);
userDBUpdate = Q.nbind(userDB.update, userDB);

  // {
  //   classID: webgisID,
  //   secret: '111',
  //   ghRegName: '运输监控平台',//微信平台注册名称
  //   ghName: '管理信息系统课',
  //   tip : '',
  //   creator: '张全志2'
  // }
var classDB  = new Datastore({ filename: 'class.db', autoload: true });
classDBFind = Q.nbind(classDB.find, classDB);
classDBFindOne = Q.nbind(classDB.findOne, classDB);
classDBRemove = Q.nbind(classDB.remove, classDB);
classDBInsert = Q.nbind(classDB.insert, classDB);
classDBUpdate = Q.nbind(classDB.update, classDB);



var request         = require('request');
httpRequestGet      = Q.denodeify(request.get);
httpRequestPost     = Q.denodeify(request.post);

var express         = require('express');
var routes          = require('./routes');
var userRoute      = require('./routes/user');
var classRoute      = require('./routes/class');
var http            = require('http');
var path            = require('path');
var importMsg = require('./routes/importMsg');

var wsServer = require('./routes/wsServer');

var app = express();

var server = wsServer.startWebSocketServer(app);

// all environments
app.set('port', process.env.PORT || 80);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(function(req, res, next){
  // console.dir(req.headers);
  if (req.is('text/*')) {
    req.rawBody = '';
    req.setEncoding('utf8');
    req.on('data', function(chunk){ req.rawBody += chunk });
    req.on('end', next);
  } else {
    next();
  }
});
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser()); 
app.use(express.session({secret: "andylau"}));
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/weixin', routes.weinCheck);
app.post('/weixin', routes.receiveMsg);
app.post('/getTucao',routes.getTucao);
app.get('/tucao', routes.tucaoIndex);
app.get('/tucaoall', routes.tucaoAjax);
app.get('/', routes.loginIndex);
app.get('/login', routes.loginIndex);
// app.get('/userList4Login', userRoute.userList4Login);
app.post('/checkLogin', userRoute.checkLogin);
app.get('/userManageIndex', userRoute.userManageIndex);
app.get('/userManageLeftIndex', userRoute.userManageLeftIndex);
app.get('/userManageTopIndex', userRoute.userManageTopIndex);
app.get('/userManageUserIndex', userRoute.userManageUserIndex);
app.get('/userManageChangepwdIndex', userRoute.userManageChangepwdIndex);
app.get('/setSpeakerIndex', userRoute.setSpeakerIndex);
app.get('/setUserStateIndex', userRoute.setUserStateIndex);
app.post('/setSpeaker', userRoute.setSpeaker);
app.post('/blockSpeak', userRoute.blockSpeak);
app.post('/userList4Speaker', userRoute.userList4Speaker);
app.post('/userList4BlockState', userRoute.userList4BlockState);
app.post('/postNewPassword', userRoute.postNewPassword);

app.get('/classManage', classRoute.classManageIndex);
app.get('/classManageLeftIndex', classRoute.classManageLeftIndex);
app.get('/classManageTopIndex', classRoute.classManageTopIndex);
app.get('/classManageClassIndex', classRoute.classManageClassIndex);
app.get('/classManageChangepwdIndex', classRoute.classManageChangepwdIndex);
Pst('/classList', classRoute.classList);
Pst('/setClassStatusOk', classRoute.setClassStatusOk);
Pst('/deleteClass', classRoute.deleteClass);
Get('/classApplyIndex', classRoute.classApplyIndex);
Pst('/classApply', classRoute.classApply);
Get('/classNameList', classRoute.classNameList);


server.listen(app.get('port'), function(){
  console.log(('weixin tucao server listening on port ' + app.get('port')).info);
});

function Get(path, pathRoute){
  app.get(path, pathRoute);
}
function Pst(path, pathRoute){
  app.post(path, pathRoute);
}




